/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020-2030 郑庚伟 ZHENGGENGWEI (码匠君), <herodotus@aliyun.com> Licensed under the AGPL License
 *
 * This file is part of Dante Engine.
 *
 * Dante Engine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dante Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.herodotus.cn>.
 */

package cn.herodotus.engine.assistant.definition.constants;

/**
 * @author gengwei.zheng
 */
public interface SymbolConstants {

    String AMPERSAND = "&";

    String AMPERSAND_ENCODED = "&amp;";

    String APOSTROPHE = "'";

    String APOSTROPHE_AND_COMMA = "',";

    String APOSTROPHE_AND_COMMA_AND_APOSTROPHE = "','";

    String AT = "@";

    String BACK_SLASH = "\\";

    String BETWEEN = "BETWEEN";

    String BLANK = "";

    String CDATA_OPEN = "<![CDATA[";

    String CDATA_CLOSE = "]]>";

    String CLOSE_BRACKET = "]";

    String CLOSE_CURLY_BRACE = "}";

    String CLOSE_PARENTHESIS = ")";

    String COLON = ":";

    String COMMA = ",";

    String COMMA_AND_APOSTROPHE = ",'";

    String COMMA_AND_SPACE = ", ";
    String SEMICOLON_AND_SPACE = "; ";

    String DASH = "-";

    String DOUBLE_APOSTROPHE = "''";

    String DOUBLE_CLOSE_BRACKET = "]]";

    String DOUBLE_OPEN_BRACKET = "[[";

    String DOUBLE_SLASH = "//";

    String EQUAL = "=";

    String GREATER_THAN = ">";

    String GREATER_THAN_OR_EQUAL = ">=";

    String FORWARD_SLASH = "/";

    String FOUR_SPACES = "    ";

    String FINISH_LEFT_ANGLE = "</";

    String FINISH_RIGHT_ANGLE = "/>";

    String GBK = "GBK";

    String IS_NOT_NULL = "IS NOT NULL";

    String IS_NULL = "IS NULL";

    String IN = "IN";

    String LEFT_ANGLE = "<";

    String LESS_THAN = "<";

    String LESS_THAN_OR_EQUAL = "<=";

    String LIKE = "LIKE";

    String MINUS = "-";

    String NBSP = "&nbsp;";

    String NEW_LINE = "\n";

    String NOT_EQUAL = "!=";

    String DB_NOT_EQUAL = "<>";

    String NOT_LIKE = "NOT LIKE";

    String NULL = "null";

    String OPEN_BRACKET = "[";

    String OPEN_CURLY_BRACE = "{";

    String OPEN_PARENTHESIS = "(";

    String PERCENT = "%";

    String PERIOD = ".";

    String PIPE = "|";

    String PLUS = "+";

    String POUND = "#";

    String QUESTION = "?";

    String QUOTE = "\"";

    String RETURN = "\r";

    String RETURN_NEW_LINE = "\r\n";

    String RIGHT_ANGLE = ">";

    String SEMICOLON = ";";

    String SLASH = FORWARD_SLASH;

    String SPACE = " ";

    String STAR = "*";

    String TAB = "\t";

    String TILDE = "~";

    String UNDERLINE = "_";

    String ZERO = "0";

    String SUFFIX_EXCEL_2003 = ".xls";

    String SUFFIX_EXCEL_2007 = ".xlsx";

    String SUFFIX_JPEG = ".jpg";

    String SUFFIX_XML = ".xml";

    String SUFFIX_PDF = ".pdf";

    String SUFFIX_ZIP = ".zip";

    String SUFFIX_DOC = ".doc";

    String SUFFIX_DOCX = ".docx";

    String SUFFIX_PPT = ".ppt";

    String SUFFIX_PPTX = ".pptx";

    String SUFFIX_EXCEL = ".xls";

    String SUFFIX_EXCELX = ".xlsx";

    String SUFFIX_SWF = ".swf";

    String SUFFIX_PROPERTIES = ".properties";

    String SUFFIX_YML = ".yml";

    String SUFFIX_YAML = ".yaml";

    String SUFFIX_JSON = ".json";

    String XML_DECLARATION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
}
