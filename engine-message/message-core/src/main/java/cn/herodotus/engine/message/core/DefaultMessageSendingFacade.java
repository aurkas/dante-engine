/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020-2030 郑庚伟 ZHENGGENGWEI (码匠君), <herodotus@aliyun.com> Licensed under the AGPL License
 *
 * This file is part of Dante Engine.
 *
 * Dante Engine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dante Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.herodotus.cn>.
 */

package cn.herodotus.engine.message.core;

import cn.herodotus.engine.message.core.constants.MessageConstants;

/**
 * <p>Description: 默认消息发送器 </p>
 *
 * @author : gengwei.zheng
 * @date : 2023/10/26 21:29
 */
public class DefaultMessageSendingFacade extends MessageSendingFacade {

    /**
     * 发送 WebSocket 给指定用户
     *
     * @param user    用户唯一标识
     * @param payload 消息内容
     */
    public static void toUser(String user, Object payload) {
        pointToPoint(user, MessageConstants.WEBSOCKET_DESTINATION_PERSONAL_MESSAGE, payload);
    }

    /**
     * 发送公告信息
     *
     * @param payload 消息内容
     */
    public static void announcement(Object payload) {
        broadcast(MessageConstants.WEBSOCKET_DESTINATION_BROADCAST_NOTICE, payload);
    }
}
